"use strict";
/* eslint-disable no-console */
/* exported whoami echo*/
/**/
////////////////////////////////////////////////////////////////////////////////
// CONSTANTES / CONFIGURATION
////////////////////////////////////////////////////////////////////////////////
const local_server = "https://localhost:8443/";
const lifap5_server = "https://lifap5.univ-lyon1.fr/";
const server = lifap5_server || local_server;

const local_todos = "./Projet-2019-topics.json";
const local_users = "./Projet-2019-users.json";

const content_type_header = "Content-Type";
const content_type_value = "application/json";
const x_api_key_header = "x-api-key";

////////////////////////////////////////////////////////////////////////////////
// ETAT : classe pour gérer l'état courant de l'application
////////////////////////////////////////////////////////////////////////////////
function State(users = [], topics = [], filters = [], sort ='Sujet croissant',sort_posts ='Date croissant', x_api_key = "ICI_VOTRE_CLEF_D_API_DE_TOMUSS"){
  // vous êtes libres d'adapter cette classe avec d'autres paramètres pour refléter l'état courant
  this.users      = users;
  this.topics     = topics;
  this.filters    = filters;
  this.sort       = sort;
  this.sort_posts = sort_posts;
  this.x_api_key  = x_api_key;
}//end State


////////////////////////////////////////////////////////////////////////////////
// OUTILS : fonctions outils, manipulation et filtrage de TODOs
////////////////////////////////////////////////////////////////////////////////

// TYPE DE TRIE POUR LES SUJETS DE DEBATS
function trie_sujet_decroissant(topics) {
	const res = Array.from(topics);
     res.sort((n1, n2) => n1.topic < n2.topic ? 1 : n1.topic > n2.topic ? -1 : 0);
     return res;
 }


function trie_sujet_croissant(topics) {
	const res = Array.from(topics);
     res.sort((n1, n2) => n1.topic > n2.topic ? 1 : n1.topic < n2.topic ? -1 : 0);;
     return res;
 }

function trie_date_croissant(topics)
{
  const res = Array.from(topics);
  res.sort((n1, n2) => n1.date > n2.date ? 1 : n1.date < n2.date ? -1 : 0);
 return res;
}

function trie_date_decroissant(topics)
{
  const res = Array.from(topics);
  res.sort((n1, n2) => n1.date < n2.date ? 1 : n1.date > n2.date ? -1 : 0);
 return res;
}

function trie_nb_croissant(topics)
{
  const res = Array.from(topics);
  res.sort((n1, n2) => n1.posts.length > n2.posts.length ? 1 : n1.posts.length < n2.posts.length ? -1 : 0);
 return res;
}

function trie_nb_decroissant(topics)
{
  const res = Array.from(topics);
  res.sort((n1, n2) => n1.posts.length < n2.posts.length ? 1 : n1.posts.length > n2.posts.length ? -1 : 0);
 return res;
}
//TYPES DE TRIE POUR LES POSTS 
function trie_likes_decroissant(posts) {
	const res = Array.from(posts);
     res.sort((n1, n2) => n1.likers.length < n2.likers.length ? 1 : n1.likers.length > n2.likers.length ? -1 : 0);
     return res;
 }
 
function trie_likes_croissant(posts) {
	const res = Array.from(posts);
     res.sort((n1, n2) => n1.likers.length > n2.likers.length ? 1 : n1.likers.length < n2.likers.length ? -1 : 0);;
     return res;
 }

function trie_dislikes_decroissant(posts) {
	const res = Array.from(posts);
     res.sort((n1, n2) => n1.dislikers.length < n2.dislikers.length ? 1 : n1.dislikers.length > n2.dislikers.length ? -1 : 0);
     return res;
 }
 
function trie_dislikes_croissant(posts) {
	const res = Array.from(posts);
     res.sort((n1, n2) => n1.dislikers.length > n2.dislikers.length ? 1 : n1.dislikers.length < n2.dislikers.length ? -1 : 0);;
     return res;
 }

function trie_datep_croissant(posts)
{
  const res = Array.from(posts);
  res.sort((n1, n2) => n1.date > n2.date ? 1 : n1.date < n2.date ? -1 : 0);
 return res;
}

function trie_datep_decroissant(posts)
{
  const res = Array.from(posts);
  res.sort((n1, n2) => n1.date < n2.date ? 1 : n1.date > n2.date ? -1 : 0);
 return res;
}

function trie_auteur_croissant(posts)
{
  const res = Array.from(posts);
  res.sort((n1, n2) => n1.user > n2.user ? 1 : n1.user < n2.user ? -1 : 0);
 return res;
}

function trie_auteur_decroissant(posts)
{
  const res = Array.from(posts);
  res.sort((n1, n2) => n1.user < n2.user ? 1 : n1.user > n2.user ? -1 : 0);
 return res;
}

function select_trie_posts(state,topics)
{
   state.sort_posts=document.getElementById("select_trie_posts").value;
   console.log(state.sort_posts);
   let key=state.topics.findIndex(x=>x.topic===topics.topic);
   let trie=state.topics[key].posts;

   if(state.sort_posts==="Likes croissant"){
    state.topics[key].posts= trie_likes_croissant(trie);
    return state.topics[key].posts;
   }

   if (state.sort_posts==="Likes décroissant"){
    state.topics[key].posts= trie_likes_decroissant(trie);
    return state.topics[key].posts;
   }

   if(state.sort_posts==="Dislikes croissant"){
    state.topics[key].posts= trie_dislikes_croissant(trie);
    return state.topics[key].posts;
   }

   if (state.sort_posts==="Dislikes décroissant"){
    state.topics[key].posts= trie_dislikes_decroissant(trie);
    return state.topics[key].posts;
   }

   if(state.sort_posts==="Date croissant"){
    state.topics[key].posts= trie_datep_croissant(trie);
    return state.topics[key].posts;
   }

   if (state.sort_posts==="Date décroissant")
   	{console.log("okp4");
    state.topics[key].posts= trie_datep_decroissant(trie);
    return state.topics[key].posts;
    }

    if(state.sort_posts==="Auteur croissant"){
    state.topics[key].posts= trie_auteur_croissant(trie);
    return state.topics[key].posts;
    }

   if (state.sort_posts==="Auteur décroissant"){
    state.topics[key].posts= trie_auteur_decroissant(trie);
    return state.topics[key].posts;
    }
}


function select_trie(state)
{
   state.sort=document.getElementById("select_trie").value;
   console.log(state.sort);
   let trie=state.topics;

   if(state.sort==="Sujet croissant"){
    state.topics= trie_sujet_croissant(trie);
    return topics;
   }
   if (state.sort==="Sujet décroissant"){
    state.topics= trie_sujet_decroissant(trie);
    return state.topics;
   }
   if(state.sort==="Date croissant"){
    state.topics= trie_date_croissant(trie);
    return state.topics;
   }
   if (state.sort==="Date décroissant"){
    state.topics= trie_date_decroissant(trie);
    return state.topics;
    }
    if(state.sort==="Nb croissant"){
    state.topics= trie_nb_croissant(trie);
    return state.topics;
   }
   if (state.sort==="Nb décroissant"){
    state.topics= trie_nb_decroissant(trie);
    return state.topics;
    }
}

//supprimer tous les handlers (et ceux de ses enfants) en clonant un élément
function clearEventListenerById(id){
  let elt = document.getElementById(id);
  let clone = elt.cloneNode(true);
  elt.parentNode.replaceChild(clone, elt);
  return id;
}

////////////////////////////////////////////////////////////////////////////////
// RENDU : fonctions génération de HTML à partir des données JSON
////////////////////////////////////////////////////////////////////////////////

function like(){ 
 document.getElementById("").innerHTML=likes; 
 likes=likes+1;
}

function dislike(){  
 document.getElementById("").innerHTML=dislikes; 
 dislikes=dislikes+1;
}
//fonction pour qui affiche en html un topic(titre ,nb de contributions et date)
function formate_sujet(topics) {
	let nb=topics.posts.length;
  return `<div class="col-8" id="${topics.topic}">${topics.topic}</div>
  <div class="col-1">${nb}</div>
  <div class="col-3">${new Date(topics.date).getFullYear()}`+"-"+
  `${new Date(topics.date).getMonth()+1}`+"-"+`${new Date(topics.date).getDate()}</div>`;
}

function formate_posts(posts) {
  return `<div>
          <a href="#">${posts.user}</a> le ${new Date(posts.date).getFullYear()}`+"-"+
          `${new Date(posts.date).getMonth()+1}`+"-"+`${new Date(posts.date).getDate()}
          (${posts.likers.length}<i class="far fa-thumbs-up"></i>) (${posts.dislikers.length}
           <i class="far fa-thumbs-down" id="${posts.content}"></i>) 
          <p class="speech-bubble">${posts.content}</p>
          </div>`;
}

function liste_posts_html(posts) {
   let posts_html =posts
       .map(x=>formate_posts(x))
       .join('\n');	
  return  `${posts_html}`;
}  

function aff_posts(state,topics)//pour afficher les posts du topics selectionnées
{
	 let x=topics;
	document.getElementById(x.topic).style.backgroundColor='#ddeeff';
         document.getElementById(x.topic).style.hover='#ddeeff';
  	     document.getElementById("select_topic").innerHTML=x.topic;
  	     document.getElementById("contrib-description").innerHTML=
  	      `<li class="list-group-item">Proposé par : <a href="#">${x.user}</a></li>
           <li class="list-group-item">Proposé le :  ${Number(new Date(x.date).getDate())}`+"/"+
           `${Number(new Date(x.date).getMonth()+1)}`+"/"+`${Number(new Date(x.date).getFullYear())} `+
           "à"+` ${Number(new Date(x.date).getHours())}`+"h"+`${Number(new Date(x.date).getMinutes())}
           </div><div class="col-3"> </li>
           <li class="list-group-item">Description : ${x.desc} </li>`;
         document.getElementById("contrib-list").innerHTML=liste_posts_html(x.posts);
         
}



function addTopic(state){
	if(state.x_api_key!==""||state.x_api_key!=="ICI_VOTRE_CLEF_D_API_DE_TOMUSS" ){
       const topic =document.getElementById("upload-title").value;
       const content =document.getElementById("upload-description").value;
     let us;
     whoami(state)().then(data=> us=data.login).catch(console.error);
       console.log(us);
       if(topic !== "" && content !== ""){
            let ajout={
           "_id" : state.x_api_key,
            "topic" : topic,
            "open" : true,
            "desc" : content,
            "date" : new Date(),
            "user" : "",
            "posts" : []

         };
             echa(state)(ajout).then(alert("Votre sujet a bien été ajouté!!")).catch(console.error);
        }
    }
}


//fonction pour ajouter l'element posts dans chaque topics 

function add_post(topics){
	topics.map(x=> x.posts=[]);
	topics.map(get_posts);
	topics.map(x=> x.posts[0]);
	return topics;
}

//Fonction qui affiche en html la liste de tous les topics
function liste_topics_html(state) {
   let topics_html =Array(state.topics)
       .map(x=>x.map(formate_sujet))
       .map(x=>x.join('\n'));	
  return `<div class="row">\n${topics_html}</div>\n`;
}  

////////////////////////////////////////////////////////////////////////////////
// HANDLERS : gestion des évenements de l'utilisateur dans l'interface HTML
////////////////////////////////////////////////////////////////////////////////
function attach_all_handlers(state){
  //on commence par tout vider
  const ids = [
    "identification-link",
    "search-btn",
    "upload-btn",
    "contrib-btn"
  ];
  ids.map(clearEventListenerById);

  document.getElementById("identification-link")
  .addEventListener("click", () => {
    // on modifie l'état
    state.x_api_key = prompt("Saisissez votre clef d'API");
    whoami(state)()
    .then(data => {
      if(data)
       alert(`Vous êtes ${data.login}`); 
         
    })
    .catch(console.error);
    //on réattache les handlers pour qu'ils utilisent tous le nouvel état
    return attach_all_handlers(state);
  }); 
  
  document.getElementById("search-btn")
  .addEventListener("click", () => document.getElementById("search-text").value);
  
  document.getElementById("contrib-btn")
  .addEventListener("click", () => alert(document.getElementById("contrib-text").value)); 
    
   //Affichage de la liste de topics
  document.getElementById("table-topics-body").innerHTML = liste_topics_html(state);

  
  document.getElementById("trie")
  .addEventListener("click",()=> {
  	select_trie(state);
    return attach_all_handlers(state); });
   
  //Pour l'affichage d'un topic selectionné
   state.topics.map(x =>
   document.getElementById(x.topic)
  .addEventListener("click",()=>{
   document.getElementById("trie_posts")
        .addEventListener("click",()=> {
  	     select_trie_posts(state,x);
  	     aff_posts(state,x);
  	     return attach_all_handlers(state);
  	      });
    aff_posts(state,x);
    return attach_all_handlers(state);}));
  
  //Pour la creation de topics
   document.getElementById("upload-btn").addEventListener("click", ()=>{
   	addTopic(state);
    return attach_all_handlers(state);
  });

  // renvoie l'état pour pouvoir être chainée

  return state;
}


////////////////////////////////////////////////////////////////////////////////
// FETCH Fonction permettant de charger des données asynchrones
////////////////////////////////////////////////////////////////////////////////
function get_local_todos() {
  return fetch(local_todos)
    .then(response => response.json());
}

function get_local_users() {
  return fetch(local_users)
    .then(response => response.json());
}

//charger les id des topics depuis le server
function get_id(){
	 const url = server + "topics";
	 let response_ok = false;
     return fetch(url,{ method: "GET"})
       .then(response => {
        response_ok= response.ok;
        return response.json();
        })
       .then(data => {
        if (response_ok) {
        return data;
        } else {
         throw(new Error(`Erreur dans la requête ${url}: ${JSON.stringify(data)}`));
        }
       });
}

//charger le contenu de chaque topics avec son id
function get_topics(values){
	 const url = server + "topics/"+values._id;
	 let response_ok = false;
  return fetch(url)
       .then(response => {
        response_ok= response.ok;
        return response.json();
        })
       .then(data => {
        if (response_ok) {
        return data;
        } else {
         throw(new Error(`Erreur dans la requête ${url}: ${JSON.stringify(data)}`));
        }
       });
	  
}


//charger les login de chaque user
function get_login(){
	 const url = server + "users";
	 let response_ok = false;
return fetch(url)
       .then(response => {
        response_ok= response.ok;
        return response.json();
        })
       .then(data => {
        if (response_ok) {
        return data;
        } else {
         throw(new Error(`Erreur dans la requête ${url}: ${JSON.stringify(data)}`));
        }
       });
	  
}


//charger le nom des tous les utilisateurs
function get_users(value){
	 const url = server + "user/"+value;
	 let response_ok = false;
	 return fetch(url)
     .then(response => {
        response_ok= response.ok;
        return response.json();
        })
       .then(data => {
        if (response_ok) {
        return data;
        } else {
         throw(new Error(`Erreur dans la requête ${url}: ${JSON.stringify(data)}`));
        }
       });
}

//charger les posts (Cette mééthode n'est pas très adéquates pour l'instant)
function get_posts(topics){
	 const url = server + "topic/"+topics._id+"/post/";
	 let response_ok = false;
	 let promise=fetch(url)
     .then(response => {
        response_ok= response.ok;
        return response.json();
        })
       .then(data => {
        if (response_ok) {
        return data;
        } else {
         throw(new Error(`Erreur dans la requête ${url}: ${JSON.stringify(data)}`));
        }
       });
       topics.posts;

        Promise.all([promise])
        .then(values => values=values[0])
        .then(values =>topics.posts=values)
       .catch(reason => console.error(reason));
       
}



//SERT POUR L'IDENTIFICATION DE L'UTILISATEUR
const whoami = (state) => () => {
  const url = server + "user/whoami";
  let headers = new Headers();
  headers.set(x_api_key_header, state.x_api_key);
  headers.set(content_type_header, content_type_value);

  let response_ok = false;

  //response.json() renvoie une promesse du corps de la réponse HTTP parsé en json
  // si la requête a réussi (response.ok) elle contient les informations de l'utilisateur authentifié avec sa clef x-api-key
  // sinon (!response.ok) la requête contient un code et une message d'erreur en JSON
  return fetch(url, { method: "GET", headers: headers })
  
  .then(response => {
    response_ok= response.ok;
    return response.json();
  })
  .then(data => {
    if (response_ok) {
      return data;
    } else {
      throw(new Error(`Erreur dans la requête ${url}: ${JSON.stringify(data)}`));
    }
  });
};

 
//Pour crééer un topics 
const echa = (state) => (data) => {
  const url = server + "topics/create";
  const body = JSON.stringify(data);
  let headers = new Headers();
  headers.set(x_api_key_header, state.x_api_key);
  headers.set(content_type_header, content_type_value);
  //headers.set(content_type_header, content_type_value);

  return fetch(url, { method: "POST", headers: headers, body: body })
      .then(function(response) {
    if (response.ok)
      return response.json();
  })
  .catch(console.error); 
};

//...
const echo = (_state) => (data) => {
  const url = server + "echo";
  const body = JSON.stringify(data);
  let headers = new Headers();
  headers.set(content_type_header, content_type_value);

  return fetch(url, { method: "POST", headers: headers, body: body })
  .then(function(response) {
    if (response.ok)
      return response.json();
  })
  //ce catch ca attraper à la fois les promesses rejetées ET les exceptions
  //on aurait pu écrire reason => console.error(reason), ce qui revient au même
  .catch(console.error); 
};

/************************************************************** */
/** MAIN PROGRAM */
/************************************************************** */
document.addEventListener("DOMContentLoaded", function(){
  if (!document.getElementById("title-test-projet")) {
    let topics;
    Promise.all([get_id()])
    .then(values => Promise.all(values[0].map(get_topics)))
    .then(values => add_post(values))
    .then(values => topics=values)//Je récupere l'objet topics
    .catch(reason => console.error(reason));

	Promise.all([get_login()])
    .then(values =>Promise.all(values[0].map(get_users)))
    .then(values => new State(values,topics))//je rempli les users ,topics et posts avec ce que j'ai recuperer
    .then(attach_all_handlers)
    .then(state => console.log(state))
    .catch(reason => console.error(reason)); 
  }
}, false);
